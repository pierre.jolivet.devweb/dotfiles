-- In order to display LSP diagnostics on hover and not inline
vim.cmd([[autocmd! CursorHold,CursorHoldI * lua vim.diagnostic.open_float(nil, {focus = false})]])

-- In order to link file type Java to jdtls & associated conf
vim.cmd([[
augroup jdtls_lsp
    autocmd!
    autocmd FileType java lua require'jdtls.jdtls_setup'.setup()
augroup end
]])
